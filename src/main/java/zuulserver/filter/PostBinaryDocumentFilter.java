package zuulserver.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import java.util.*;

public class PostBinaryDocumentFilter extends ZuulFilter {

    RequestContext ctx;
    String req;
    private final String FUNCTION_CODE = "abdWtq57DbhPebN0Te0y6u/MP2TO1UTLyKawHJCmvdbOZ/mPaAjQpg==";

    public PostBinaryDocumentFilter() {
    }

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 6;
    }

    @Override
    public boolean shouldFilter() {
        ctx = RequestContext.getCurrentContext();
        req = ctx.getRequest().getRequestURI();
        return req.startsWith("/api/Document/PostBinaryDocument");
    }

    @Override
    public Object run() {
        List<String> pathParts = new ArrayList<>(Arrays.asList(req.split("/")));
        String container = pathParts.get(pathParts.size() - 2);
        String prefix = pathParts.get(pathParts.size() - 1);
        Map<String, List<String>> qp = new HashMap<>();
        qp.put("code", Collections.singletonList(FUNCTION_CODE));
        qp.put("container", Collections.singletonList(container));
        qp.put("prefix", Collections.singletonList(prefix));
        ctx.setRequestQueryParams(qp);
        ctx.set("requestURI", "/");
        return null;
    }
}
