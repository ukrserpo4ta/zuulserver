package zuulserver.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetZipFilter extends ZuulFilter {

    private final String FUNCTION_CODE = "5MmFjVIxvuraTqsB7bwRJAHgbmBTn12TgikcP4U6jZd4M9SEkvvITA==";

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 6;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        String req = ctx.getRequest().getRequestURI();
        return req.startsWith("/api/Document/getZip");
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        Map<String, List<String>> qp = new HashMap<>();
        qp.put("code", Collections.singletonList(FUNCTION_CODE));
        ctx.setRequestQueryParams(qp);
        return null;
    }
}
