package zuulserver.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import java.util.*;

public class GetDocumentFilter extends ZuulFilter {

  RequestContext ctx;
  String req;
  private final String FUNCTION_CODE = "hZSkKCNL7RSa7ajsyCbtUHoCaOPjehwhMzudumjPBCwPKcKCcprY2w==";

  public GetDocumentFilter() {
  }

  @Override
  public String filterType() {
    return FilterConstants.PRE_TYPE;
  }

  @Override
  public int filterOrder() {
    return 6;
  }

  @Override
  public boolean shouldFilter() {
    ctx = RequestContext.getCurrentContext();
    req = ctx.getRequest().getRequestURI();
    return req.startsWith("/api/Document/GetDocument");
  }

  @Override
  public Object run() {
    List<String> pathParts = new ArrayList<>(Arrays.asList(req.split("/")));
    String container = pathParts.get(pathParts.size()-2);
    String document = pathParts.get(pathParts.size()-1);
    Map<String, List<String>> qp = new HashMap<>();
    qp.put("code", Collections.singletonList(FUNCTION_CODE));
    qp.put("Container", Collections.singletonList(container));
    qp.put("Document", Collections.singletonList(document));
    ctx.setRequestQueryParams(qp);
    ctx.set("requestURI", "/");
    return null;
  }
}
