package zuulserver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import zuulserver.filter.*;

@Configuration
public class ZuulFiltersConfig {

    @Bean
    public GetDocumentFilter getDocumentFilter() {
        return new GetDocumentFilter();
    }

    @Bean
    public DeleteContainerFilter deleteContainerFilter() {
        return new DeleteContainerFilter();
    }

    @Bean
    public PostDocumentFilter postDocumentFilter() {
        return new PostDocumentFilter();
    }

    @Bean
    public PostBinaryDocumentFilter postBinaryDocumentFilter() {
        return new PostBinaryDocumentFilter();
    }

    @Bean
    public CopyContainerFilter copyContainerFilter() {
        return new CopyContainerFilter();
    }

    @Bean
    public CopyFilesFilter copyFilesFilter() {
        return new CopyFilesFilter();
    }

    @Bean
    public MoveFilesFilter moveFilesFilter() {return new MoveFilesFilter();}

    @Bean
    public DeleteFileFilter deleteFileFilter() {
        return new DeleteFileFilter();
    }

    @Bean
    public RenameContainerFilter renameContainerFilter() {
        return new RenameContainerFilter();
    }

    @Bean
    public SendExecutionStatusStartedFilter sendExecutionStatusStartedFilter() {
        return new SendExecutionStatusStartedFilter();
    }

    @Bean
    public SendExecutionStatusCompleteFilter sendExecutionStatusCompleteFilter() {
        return new SendExecutionStatusCompleteFilter();
    }

    @Bean
    public SendResetPasswordFilter sendResetPasswordFilte() {
        return new SendResetPasswordFilter();
    }

    @Bean
    public GetZipFilter getZipFilter() {
        return new GetZipFilter();
    }

}


