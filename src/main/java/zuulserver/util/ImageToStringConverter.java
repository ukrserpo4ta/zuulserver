package zuulserver.util;

import org.apache.tomcat.util.codec.binary.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ImageToStringConverter {

    public static void main(String[] args) throws IOException {

        File f = new File("D:/big.jpg");
        String encodstring = encodeFileToBase64Binary(f);
        System.out.println(encodstring);
        writeStringToFile(encodstring);
    }

    private static String encodeFileToBase64Binary(File file) {
        String encodedfile = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int) file.length()];
            fileInputStreamReader.read(bytes);
            encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return encodedfile;
    }

    private static void writeStringToFile(String base64String) throws IOException {
        String path = "big.txt";
        Files.write(Paths.get(path), base64String.getBytes(), StandardOpenOption.CREATE);
    }
}
